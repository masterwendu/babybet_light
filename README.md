# babybet-light
[![Build Status](https://ci.wendelin.dev/api/badges/masterwendu/babybet_light/status.svg?ref=refs/heads/main)](https://ci.wendelin.dev/masterwendu/babybet_light)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
