module.exports = {
  purge: [
    './src/**/*.vue',
    './src/*.js',
    './src/*.vue',
    './src/*.css',
    './src/**/*.css',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
